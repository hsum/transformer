var contactSums = {};
(function countDealsSum(offset, count){
    requestApi({
        path : "/deals/v1/deal/recent/modified",
        qs : {offset : offset, count : count}
    }).then(function(data){
        var resultsCount = data.results.length;
        for(var i = 0; i < resultsCount; i++){
            var deal = data.results[i];
            if(deal.properties.amount && deal.properties.dealstage && deal.properties.dealstage.value == 'closedwon'){
                var associatedVids = deal.associations.associatedVids;
                for(j = 0; j < associatedVids.length; j++){
                    if(!contactSums[associatedVids[j]]){
                        contactSums[associatedVids[j]] = 0;
                    }
                    contactSums[associatedVids[j]] += parseFloat(deal.properties.amount.value);
                }
            }
        }
        if(resultsCount == count){
            countDealsSum(offset + count, count);
        } else {
            for(var vid in contactSums){
                var sum = contactSums[vid].toFixed(6);
                requestApi({
                    path : "/contacts/v1/contact/vid/" + vid + "/profile",
                    method : "POST",
                    body : {"properties": [
                        {"property": "total_de_ventas", "value": sum}
                    ]}
                })
            }
        }
    })
})(0, 100);