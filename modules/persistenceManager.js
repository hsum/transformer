var pg = require('pg');
var config = require('./../config/config.json');
var q = require('q');

function db(handlers){
    //store state and make a promise query
    var connection = {};
    connection.query = function(query, data){
        var self = this;
        if(data == null) data = [];
        var deferred = q.defer();
        connection.client.query(query, data, function(err, result) {
            if(err) {
                deferred.reject(err);
            } else {
                self.result = result;
                deferred.resolve(self);
            }
        });
        return deferred.promise;
    }
    connection.close = function(){
        if(this.open){
            this.done();
            //note: Clients created via the pg#connect method will be automatically disconnected or placed back into the connection pool and should not have their #end method called.
            //https://github.com/brianc/node-postgres/wiki/Client#method-end
            //this.client.end();
        }
    }

    //main promise
    var deferred = q.defer();
    pg.connect(config[global.env].pgUrl, function(err, client, done) {
        if(err) {
            deferred.reject(err);
        } else {
            connection.open = true;
            connection.client = client;
            connection.done = done;
            deferred.resolve(connection);
        }
    });
    var promise = deferred.promise;
    for(var i = 0; i < handlers.length; i++){
        promise = promise.then(handlers[i]);
    }
    return promise.fail(function (error) {
        console.error(error);
        return q.reject(error);
    }).fin(function () {
        connection.close();
    });
}
function PersistenceManager(){
}
PersistenceManager.prototype.getUser = function(id){
    return db([
        function(conn){
            return conn.query('SELECT * FROM users WHERE id = $1', [id]);
        },
        function(conn){
            return conn.result.rows.length == 0 ? null : conn.result.rows[0];
        }
    ]);
}
PersistenceManager.prototype.addUser = function(email){
    return db([
        function(conn){
            return conn.query('SELECT id FROM users WHERE email = $1', [email]);
        },
        function(conn){
            if(conn.result.rows.length == 0){
                return conn.query('INSERT INTO users ("email") VALUES ($1) RETURNING id', [email]);
            }
            return conn;
        },
        function(conn){
            return conn.result.rows[0].id;
        }
    ]);
}
PersistenceManager.prototype.addTransformation = function(user, name, recurrence, script, active){
    return db([
        function(conn){
            return conn.query('INSERT INTO transformations ("user", "name", "recurrence", "script", "active") VALUES ($1, $2, $3, $4, $5) RETURNING id',
                [user, name, recurrence, script, active]);
        },
        function(conn){
            return conn.result.rows[0].id;
        }
    ]);
}
PersistenceManager.prototype.updateTransformation = function(id, user, name, recurrence, script, active){
    return db([
        function(conn){
            return conn.query('UPDATE transformations SET "name" = $1, "recurrence" = $2, "script" = $3, "active" = $4 WHERE "id" = $5 and "user" = $6',
                [name, recurrence, script, active, id, user]);
        }
    ]);
}
PersistenceManager.prototype.getTransformations = function(user){
    return db([
        function(conn){
            return conn.query('SELECT id, name FROM transformations WHERE "user" = $1' , [user]);
        },
        function(conn){
            return conn.result.rows;
        }
    ]);
}
PersistenceManager.prototype.getTransformationsByRecurrences = function(recurrences){
    return db([
        function(conn){
            var params = [];
            for(var i = 1; i <= recurrences.length; i++) {
                params.push('$' + i);
            }
            var values = recurrences.slice();
            values.push(true);
            return conn.query('SELECT id, name, script FROM transformations WHERE "recurrence" IN (' + params.join(',') + ') and "active" = $' + (recurrences.length + 1) , values);
        },
        function(conn){
            return conn.result.rows;
        }
    ]);
}
PersistenceManager.prototype.getTransformation = function(id, user){
    return db([
        function(conn){
            return conn.query('SELECT * FROM transformations WHERE id = $1 and "user" = $2', [id, user]);
        },
        function(conn){
            return conn.result.rows.length == 0 ? null : conn.result.rows[0];
        }
    ]);
}
PersistenceManager.prototype.deleteTransformation = function(id, user){
    return db([
        function(conn){
            return conn.query('DELETE FROM transformations WHERE id = $1 and "user" = $2', [id, user]);
        }
    ]);
}

PersistenceManager.prototype.addScriptExecution = function(transformation, date, status, error){
    return db([
        function(conn){
            return conn.query('INSERT INTO "scriptExecutions" ("transformation", "date", "status", "error") VALUES ($1, $2, $3, $4) RETURNING id',
                [transformation, date, status, error]);
        },
        function(conn){
            return conn.result.rows[0].id;
        }
    ]);
}
PersistenceManager.prototype.addHubspotTransaction = function(path, method, data, transformation, date, status, error){
    return db([
        function(conn){
            return conn.query('INSERT INTO "hubspotTransactions" ("path", "method", "data", "transformation", "date", "status", "error") VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id',
                [path, method, data, transformation, date, status, error]);
        },
        function(conn){
            return conn.result.rows[0].id;
        }
    ]);
}
PersistenceManager.prototype.getScriptExecutions = function(user, limit, offset){
    return db([
        function(conn){
            return conn.query('SELECT t."name", s."id", s."date", s."status", s."error" FROM "scriptExecutions" s LEFT OUTER JOIN "transformations" t ON s."transformation" = t."id" WHERE t."user" = $1 ORDER BY s."date" DESC LIMIT $2 OFFSET $3' , [user, limit, offset]);
        },
        function(conn){
            return conn.result.rows;
        }
    ]);
}
PersistenceManager.prototype.getHubspotTransactions = function(user, limit, offset){
    return db([
        function(conn){
            return conn.query('SELECT t."name", h."id", h."path", h."method", h."data", h."date", h."status", h."error" FROM "hubspotTransactions" h LEFT OUTER JOIN "transformations" t ON h."transformation" = t."id" WHERE t."user" = $1 ORDER BY h."date" DESC LIMIT $2 OFFSET $3' , [user, limit, offset]);
        },
        function(conn){
            return conn.result.rows;
        }
    ]);
}


module.exports = new PersistenceManager();