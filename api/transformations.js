var express = require('express');
var router = express.Router();
var pm = require('../modules/persistenceManager');

router.get('/', function(req, res, next) {
    pm.getTransformations(req.session.userId).then(function(transformations){
        res.json(transformations);
    });
});
router.get('/:transformationId', function(req, res, next) {
    var transformationId = req.params.transformationId;
    pm.getTransformation(transformationId, req.session.userId).then(function(transformation){
        res.json(transformation);
    });
});
router.delete('/:transformationId', function(req, res, next) {
    var transformationId = req.params.transformationId;
    pm.deleteTransformation(transformationId, req.session.userId).then(function(){
        res.json(transformationId);
    });
});
router.put('/', function(req, res, next) {
    var transformation = req.body;
    pm.updateTransformation(transformation.id, req.session.userId, transformation.name, transformation.recurrence, transformation.script, transformation.active).then(function(){
        res.json(transformation);
    });
});
router.post('/', function(req, res, next) {
    var transformation = req.body;
    pm.addTransformation(req.session.userId, transformation.name, transformation.recurrence, transformation.script, transformation.active).then(function(id){
        transformation.id = id;
        res.json(transformation);
    });
});

module.exports = router;