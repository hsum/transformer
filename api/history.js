var express = require('express');
var router = express.Router();
var pm = require('../modules/persistenceManager');

router.get('/scriptExecutions', function(req, res, next) {
    pm.getScriptExecutions(req.session.userId, req.query.limit, req.query.offset).then(function(results){
        res.json(results);
    });
});
router.get('/hubspotTransactions', function(req, res, next) {
    pm.getHubspotTransactions(req.session.userId, req.query.limit, req.query.offset).then(function(results){
        res.json(results);
    });
});
module.exports = router;