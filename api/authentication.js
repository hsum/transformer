var express = require('express');
var router = express.Router();
var pm = require('../modules/persistenceManager');

router.post('/signin', function(req, res, next) {
    var email = req.body.email;
    pm.addUser(email).then(function(userId){
        pm.getUser(userId).then(function(user){
            req.session.userId = user.id;
            req.session.user = user;
            res.json(user);
        });
    });
});

router.post('/signout', function(req, res, next) {
    req.session.destroy(function(err) {
        if (err) return next(err);
        res.end();
    })
});

module.exports = router;