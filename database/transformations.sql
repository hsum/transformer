CREATE TABLE public.transformations (
  id BIGSERIAL,
  name VARCHAR(100) NOT NULL,
  "user" BIGINT NOT NULL,
  recurrence VARCHAR(100) NOT NULL,
  script TEXT NOT NULL,
  active BOOLEAN NOT NULL,
  CONSTRAINT conferences_pkey PRIMARY KEY(id),
  CONSTRAINT conferences_fk FOREIGN KEY ("user")
    REFERENCES public.users(id)
    ON DELETE CASCADE
    ON UPDATE NO ACTION
    NOT DEFERRABLE
)
WITH (oids = false);