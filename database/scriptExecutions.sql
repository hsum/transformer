CREATE TABLE public."scriptExecutions" (
  id BIGSERIAL,
  transformation BIGINT NOT NULL,
  date TIMESTAMP(0) WITH TIME ZONE NOT NULL,
  status BOOLEAN NOT NULL,
  error TEXT,
  CONSTRAINT "scriptExecutions_pkey" PRIMARY KEY(id),
  CONSTRAINT "scriptExecutions_fk" FOREIGN KEY (transformation)
    REFERENCES public.transformations(id)
    ON DELETE CASCADE
    ON UPDATE NO ACTION
    NOT DEFERRABLE
)
WITH (oids = false);