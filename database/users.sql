CREATE TABLE public.users (
  id BIGSERIAL,
  email VARCHAR(100) NOT NULL,
  CONSTRAINT users_auth_key UNIQUE(email),
  CONSTRAINT users_pkey PRIMARY KEY(id)
)
WITH (oids = false);