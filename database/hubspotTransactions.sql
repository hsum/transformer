CREATE TABLE public."hubspotTransactions" (
  id BIGSERIAL,
  path TEXT NOT NULL,
  method VARCHAR(100) NOT NULL,
  data TEXT,
  transformation BIGINT NOT NULL,
  date TIMESTAMP(0) WITH TIME ZONE NOT NULL,
  status BOOLEAN NOT NULL,
  error TEXT,
  CONSTRAINT "hubspotTransactions_pkey" PRIMARY KEY(id),
  CONSTRAINT "hubspotTransactions_fk" FOREIGN KEY (transformation)
    REFERENCES public.transformations(id)
    ON DELETE CASCADE
    ON UPDATE NO ACTION
    NOT DEFERRABLE
)
WITH (oids = false);