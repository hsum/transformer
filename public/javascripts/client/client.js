'use strict';

angular.module('transformer', ['ngAria', 'ngRoute', 'infinite-scroll'])
    .config(['$routeProvider', function($routeProvider) {
            $routeProvider
                .when('/history', {
                    template:
                    '<div class="content holder padded">' +
                    '<div class="types"><span ng-repeat="type in types" class="link" ng-class="{underlined : type != selectedType}" ng-click="getHistory(type, false)">{{type.label}}</span></div>' +
                    '<div infinite-scroll="getHistory(selectedType, true)" infinite-scroll-disabled="loading || !hasMoreItems">' +
                    '<table class="scripts table table-striped" ng-if="selectedType.name == \'scriptExecutions\'">' +
                    '<thead><tr><th class="item0">ID</th><th class="item1">Transformation</th><th class="item2">Date</th><th class="item3">Status</th><th class="item4">Error</th></tr></thead>' +
                    '<tbody> ' +
                    '<tr ng-repeat="item in items"><td>{{item.id}}</td><td>{{item.name}}</td><td>{{item.date | date:\'yyyy-MM-dd HH:mm:ss\'}}</td><td>{{item.status ? \'Success\' : \'Failed\'}}</td><td>{{item.error}}</td></tr> ' +
                    '</tbody>' +
                    '</table>' +
                    '<table class="transactions table table-striped" ng-if="selectedType.name == \'hubspotTransactions\'">' +
                    '<thead><tr><th class="item0">ID</th><th class="item1">Transformation</th><th class="item2">Path</th><th class="item3">Method</th><th class="item4">Data</th><th class="item5">Date</th><th class="item6">Status</th><th class="item7">Error</th></tr></thead>' +
                    '<tbody> ' +
                    '<tr ng-repeat="item in items"><td>{{item.id}}</td><td>{{item.name}}</td><td>{{item.path}}</td><td>{{item.method}}</td><td>{{item.data}}</td><td>{{item.date | date:\'yyyy-MM-dd HH:mm:ss\'}}</td><td>{{item.status ? \'Success\' : \'Failed\'}}</td><td>{{item.error}}</td></tr> ' +
                    '</tbody>' +
                    '</table>' +
                    '<div ng-if="loading" class="text-center"><div class="loading status"></div></div>' +
                    '</div>',
                    controller: 'HistoryController'
                })
                .when('/transformations', {
                    template:
                    '<div class="transformations area clearfix" ng-if="dataLoaded">'+

                    '<div class="transformations listing pull-left">' +
                    '<div class="text-center">' +
                    '<a ng-click="addTransformation()" class="add btn btn-blue"><span>New Transformation</span></a>' +
                    '</div>' +
                    '<div ng-click="selectTransformation(item)" ng-repeat-start="item in transformations" class="transformation padded">{{item.name}}</div>' +
                    '<div ng-repeat-end ng-if="!$last" class="padded"><div class="horizontal divider"></div></div>' +
                    '</div>' +

                    '<div class="main space pull-right">'+
                    '<div class="vcentered center holder">'+
                    '<div>'+
                    '<form ng-if="transformation" name="forms.transformation" class="edit pane" novalidate>'+
                    '<div class="line vcentered">'+
                    '<div class="cell title"><label>Name *</label></div>'+
                    '<div class="cell control"><input class="input" type="text" ng-required="true" ng-model="transformation.name" maxlength="100"/></div>'+
                    '</div>'+
                    '<div class="line vcentered">'+
                    '<div class="cell title"><label>Recurrence</label></div>'+
                    '<div class="cell control"><select class="select_styled" ng-model="transformation.recurrence" ng-options="item.value as item.label for item in recurrenceOptions"></select></div>'+
                    '</div>'+
                    '<div class="line vcentered">'+
                    '<div class="cell title"><label>Script *</label></div>'+
                    '<div class="cell control"><textarea class="textarea" rows="5" ng-required="true" ng-model="transformation.script"></textarea></div>'+
                    '</div>'+
                    '<div class="line vcentered">'+
                    '<div class="cell title"><label>Active</label></div>'+
                    '<div class="cell control"><label><input type="checkbox" ng-model="transformation.active"/></label></div>'+
                    '</div>'+
                    '<div class="line submit clearfix">'+
                    '<span class="btn btn-blue pull-right"><input type="submit" ng-class="{disabled : saveStatus.inProgress}" ng-disabled="saveStatus.inProgress" ng-click="saveTransformation()" value="Save"/></span>'+
                    '<span class="btn btn-red pull-right"><input type="button" ng-class="{disabled : saveStatus.inProgress}" ng-disabled="saveStatus.inProgress" ng-click="deleteTransformation()" value="Delete"/></span>'+
                    '<div class="success message pull-right">{{saveStatus.message}}</div>'+
                    '</div>'+
                    '</form>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+

                    '</div>',
                    controller: 'TransformationsController'
                })
                .when('/', {
                    template: '<div class="vcentered center holder" ng-show="dataLoaded">' +
                    '<div>' +
                    '<div class="center pane" id="signinPanelRoot"></div>' +
                    '</div>' +
                    '</div>',
                    controller: 'HomeController'
                })

    }])
    .run(['$document', function($document) {
        $document.find("#page").css('visibility', 'visible');
    }])
    .controller('AppController', ['$scope', '$window', 'dataApi', '$location', function($scope, $window, dataApi, $location) {
        //-----------------------------app-wide utilities---------------------------
        //page style
        $scope.setPageStyle = function(style){
            $scope.pageStyle = style;
        }

        //page loading
        var loadedParts = 0;
        function onPagePartLoaded(){
            loadedParts++;
            if(loadedParts == 1){
                $scope.$emit('pageLoaded');
                $scope.pageLoaded = true;
            }
        }
        $scope.whenPageLoaded = function(callback){
            if($scope.pageLoaded){
                callback();
            } else {
                $scope.$on('pageLoaded', function(){
                    callback();
                });
            }
        }
        $scope.whenLoggedPageLoaded = function(callback){
            $scope.whenPageLoaded(function(){
                if($scope.user == null){
                    $location.path('/');
                } else {
                    callback();
                }
            })
        }

        //---------------------------authentication-------------------------//
        function setUser(user){
            $scope.$apply(function() {
                $scope.user = user;
                if(user != null){
                    dataApi.post('/api/authentication/signin', {email : user.email}).then(function(response){
                        onPagePartLoaded();
                    });
                } else {
                    onPagePartLoaded();
                }
            });
        }
        new HubspotHsum($window.config.hsumUrl, $window.config.hsumUrl).loadSignInModule(function(module){
            module.getUser(function(user){
                $scope.showSigninPanel = module.showSigninPanel;
                $scope.logout = function(){
                    dataApi.post('/api/authentication/signout').then(function(response){
                        module.logout();
                    });
                };
                setUser(user);
            });
        });
    }])
    .controller('HomeController', ['$scope', '$location', '$document', '$window', function($scope, $location, $document, $window) {
        $scope.setPageStyle("signin");
        $scope.whenPageLoaded(function(){
            if($scope.user == null){
                var signinPanelRoot = angular.element($document.find("#signinPanelRoot"))[0];
                $scope.showSigninPanel(signinPanelRoot, function(){
                    $window.location.reload();
                });
                $scope.dataLoaded = true;
            } else {
                $location.path('/transformations');
            }
        });
    }])
    .controller('HistoryController', ['$scope', 'dataApi', function($scope, dataApi) {
        $scope.setPageStyle("history");
        $scope.types = [
            {name : "scriptExecutions", label : "Scripts"},
            {name : "hubspotTransactions", label : "Transactions"}
        ]
        var limit = 20;
        var offset = 0;
        $scope.items = [];
        $scope.getHistory = function(type, append){
            $scope.loading = true;
            if(!append) offset = 0;
            dataApi.get('/api/history/' + type.name, {offset : offset, limit : limit}).then(function(response){
                var items = response.data;
                if(append){
                    $scope.items = $scope.items.concat(items);
                } else {
                    $scope.items = items;
                }
                offset += items.length;
                $scope.loading = false;
                $scope.hasMoreItems = items.length == limit;
                $scope.selectedType = type;
            });
        }
        $scope.whenLoggedPageLoaded(function(){
            $scope.getHistory($scope.types[0]);
        });
    }])
    .controller('TransformationsController', ['$scope', 'dataApi', 'SaveStatus', function($scope, dataApi, SaveStatus) {
        $scope.setPageStyle("transformations");
        $scope.recurrenceOptions = [
            {value: "10min", label: 'every 10 min'},
            {value: "hour", label: 'every hour'},
            {value: "day", label: 'every day'},
            {value: "week", label: 'every week'},
            {value: "month", label: 'every month'}
        ]
        $scope.forms = {};
        $scope.saveStatus = new SaveStatus();

        function resetFormState(){
            if($scope.forms.transformation){
                $scope.forms.transformation.$setPristine();
            }
        }

        $scope.addTransformation = function(){
            $scope.saveStatus.cancel();
            resetFormState();
            $scope.transformation = {
                recurrence : "day",
                active : true
            };
        }
        $scope.saveTransformation = function(){
            if($scope.forms.transformation.$invalid){
                return;
            }
            $scope.saveStatus.start();
            var newTransformation = $scope.transformation.id == null;
            dataApi[newTransformation ? 'post' : 'put']('/api/transformations', $scope.transformation).then(function(response){
                var transformation = response.data;
                if(newTransformation){
                    $scope.transformations.push(transformation);
                } else {
                    for(var i = 0; i < $scope.transformations.length; i++){
                        if($scope.transformations[i].id == transformation.id){
                            $scope.transformations[i] = transformation;
                            break;
                        }
                    }
                }
                $scope.transformation = jQuery.extend(true, {}, transformation);
                $scope.saveStatus.finish();
            });
        }
        $scope.selectTransformation = function(transformation){
            dataApi.get('/api/transformations/' + transformation.id).then(function(response){
                resetFormState();
                $scope.saveStatus.cancel();
                $scope.transformation = response.data;
            });
        }
        $scope.deleteTransformation = function(){
            $scope.saveStatus.start();
            dataApi.delete('/api/transformations/' + $scope.transformation.id).then(function(response){
                var id = response.data;
                for(var i = 0; i < $scope.transformations.length; i++){
                    if($scope.transformations[i].id == id){
                        $scope.transformations.splice(i, 1);
                        break;
                    }
                }
                $scope.transformation = null;
                $scope.saveStatus.finish();
            });
        }

        $scope.whenLoggedPageLoaded(function(){
            dataApi.get('/api/transformations').then(function(response){
                $scope.transformations = response.data;
                $scope.dataLoaded = true;
            });
        });
    }])
    .factory('SaveStatus', ['$timeout', function($timeout) {
        function SaveStatus(){
        }
        SaveStatus.prototype.cancel = function(){
            this.inProgress = false;
            this.message = null;
        }
        SaveStatus.prototype.start = function(){
            this.inProgress = true;
            this.message = null;
        }
        SaveStatus.prototype.finish = function(){
            this.inProgress = false;
            this.message = "Data has been successfully saved";
            var self = this;
            $timeout(function(){
                self.message = null;
            }, 2000);
        }
        return SaveStatus;
    }])
    .factory('dataApi', ['$http', '$q', '$log', function($http, $q, $log) {

        function success(response){
            return response;
        }
        function error(response){
            $log.error("Request to the server failed");
            return $q.reject(response);
        }

        return {
            get : function(path, params, properties){
                var httpProperties = jQuery.extend({
                    method : "GET",
                    url: path,
                    params : params
                }, properties);
                return $http(httpProperties).then(success, error);
            },
            post : function(path, data){
                return $http({
                    method : "POST",
                    url: path,
                    data : data
                }).then(success, error);
            },
            put : function(path, data){
                return $http({
                    method : "PUT",
                    url: path,
                    data : data
                }).then(success, error);
            },
            delete : function(path){
                return $http({
                    method : "DELETE",
                    url: path
                }).then(success, error);
            }
        }
    }])
;
